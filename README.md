# Octopus profiling slow down investigation
The mpi builders extremely long runtime: 1 hour in [spack_foss-2022a_mpi/40](https://www.octopus-code.org/buildbot/#/builders/102/builds/40) and 24 minutes in [spack_foss-2022a_mpi_wo_codecov2](https://www.octopus-code.org/buildbot/#/builders/170/builds/2) is being debugged in this repository.

  The diff of the two config.h files for the above builds can be found [here}(https://github.com/iamashwin99/octopus-buildbot-config-checker/blob/main/spack_mpi_withandwithout_profiling.ipynb).

All the analysis is done in the `runs` directory. The `runs` directory contains the following files:
- Analysis.ipynb : This is the main analysis file. It contains the analysis of the runs
- Makefile : This is the makefile used to run the runs on slurm (has targets 2021a,2022a, 2022a-serial for spack and eb for easybuild runs)
- commonbuild.sh: a configure wrapper that works for all spack variants
- commonbuild_eb.sh: a configure wrapper that works for all spack variants
- full_data.csv : This is the data file that contains the data for all the runs (backup from one of the runs might be outdated)
- runscript.py : This is the script that creates the slrum scripts and runs them.

To run these builds, you can specify the run number, and a metadata for the run in the Makefile and type:
```
make 2022a-serial
#or
make eb
#or more generally
dry_run="--dry_run" run_id="3" metadata="eb_test" make eb
#or in production
run_id="3" metadata="eb_prod" make eb
```
Note: all eb runs need to haveeb in their metadata for easy identification in analysis.
see this code :
```python
      system = "eb" if "_eb_" in str(run) else "spack"
      type_of_run = "serial" if "s_serial" in str(run) else "mpi"
```
# runscript
A python script that takes :
- run number : A number representing run
- metadata: Arbitrary metadata for inforamtion
- profiling: A boolean to enable profiling
- oct_profiling: A boolean to enable octopus profiling
- toolchain: 2021a or 2022a
- dry_run: A boolean to print the slurm script but do not submit it
- eb: A boolean to run on eb toolchain
- serial: A boolean to run on serial toolchain (only spack toolchain)
and then generates and submits a slurm job to compile and run the octopus code checks

# Reproduce the results
- clone octopus repo
- checkout to the required commit
- remove .git
- checkout this repo somwehere and copy all the contents of this repo to the root of octopus repo
- ensure the runs directory is in `$octopus_root/runs`
- the previous steps are important as this repo contains modified testsuite scripts that keep the makecheck run data in the `makecheckdump` directory instead of deleteing it after the run
- inside the runs directory, run `make 2022a-serial` or `make eb` or other targets to run the builds (after updating the metadata in the Makefile)
- Run the analysis.ipynb to get the results
# Analysis
The notebooks consist of two analyses:
- run times of all the variants (matrix of different values for: spack, eb, serial, mpi, profiling, octopus profiling)
- comparing the run times of each test for profiling and profiling runs

## Results
At the time of writing this, the results for run times are as follows:

| run_name                                      | toolchain | profile | octopus_profile | eb         | run_time  |
|-----------------------------------------------|-----------|---------|-----------------|------------|-----------|
| 2_tmp_2021a_no_profile_nooctprof-187083.logit | 2021a     | 0       | 0               | spack      | 00:16:55  |
| 2_tmp_2021a_profile_nooctprof-187084.logit    | 2021a     | 1       | 0               | spack      | 00:56:24  |
| 2_tmp_2021a_profile_octprof-187086.logit      | 2021a     | 1       | 1               | spack      | 02:37:30  |
| 2_tmp_2021a_no_profile_octprof-187085.logit    | 2021a     | 0       | 1               | spack      | 03:53:43  |
| 2_s_serial_2022a_no_profile_nooctprof-187144.logit | 2022a | 0       | 0               | spack      | 00:12:39  |
| 2_eb_2022a_no_profile_nooctprof-187103.logit   | 2022a     | 0       | 0               | easybuild  | 00:15:34  |
| 2_tmp_2022a_no_profile_nooctprof-187087.logit  | 2022a     | 0       | 0               | spack      | 00:16:12  |
| 2_eb_2022a_profile_nooctprof-187104.logit      | 2022a     | 1       | 0               | easybuild  | 00:16:42  |
| 2_s_serial_2022a_profile_nooctprof-187145.logit | 2022a | 1       | 0               | spack      | 00:19:37  |
| 2_tmp_2022a_profile_nooctprof-187088.logit     | 2022a     | 1       | 0               | spack      | 00:56:16  |
| 2_eb_2022a_no_profile_octprof-187105.logit     | 2022a     | 0       | 1               | easybuild  | 01:00:28  |
| 2_eb_2022a_profile_octprof-187106.logit        | 2022a     | 1       | 1               | easybuild  | 01:02:25  |
| 2_tmp_2022a_no_profile_octprof-187089.logit    | 2022a     | 0       | 1               | spack      | 01:49:24  |
| 2_s_serial_2022a_no_profile_octprof-187146.logit | 2022a | 0       | 1               | spack      | 01:57:49  |
| 2_s_serial_2022a_profile_octprof-187147.logit  | 2022a     | 1       | 1               | spack      | 02:31:29  |
| 2_tmp_2022a_profile_octprof-187090.logit       | 2022a     | 1       | 1               | spack      | 04:16:29  |

see [Analysis.ipynb](runs/Analysis.ipynb) for more details and upto date results.
Here:
- runs with `tmp`, `s_serial` in the name(metadata for make file) are spack runs. `s_serial` is serial spack run
- `eb` is easybuild run
- runs with `no_profile` in the name are runs without profiling ( Compiler profiling is disabled)
- runs with `profile` in the name are runs with profiling ( Compiler profiling is enabled) (ths is used in production bb and in spack builders this is the one that is slow)
- runs with `nooctprof` in the name are runs without octopus profiling ( Octopus profiling is disabled) (enables seeing which functions are called howmany times and ran for how long for each tests in the test suite) the data for this is stored in `makecheckdump/test_name/test/profiling/time.0000`
- runs with `octprof` in the name are runs with octopus profiling ( Octopus profiling is enabled)

Conclusions:
- The compiler profiling slows down the build by several times in spack mpi builds
- The compiler profiling slows down the build by few minutes in spack serial builds
- The octopus profiling does not slow down the build by much in easybuild builds
- The codecov upload for both easybuil and spack have similar data ( spack is not doing more than it should)


# Update: 23/01/24

We noticed that the spack builds have `-pthread` but not in easybuild.
We traced this to the mpi wrappers ( mpicc, mpifort, mpic++) that add `-pthread` in spack but not in easybuild.

We also noticed that debian openmpi wrappers for eg also come with the `-pthread` indicating that it might perhaps easybuild that is inconsistent here.
But fedora openmpi wrappers do not have `-pthread` in them.

I ran a build of octopus with `-pthread` removed from all the wrappers  and the resulting binary has no `locks` and is also running at comparable wall time as from easybuild ones. (See runs `5_n_pthread*` and `6_n_pthread_a_lpthread*` in the analysis notebook)

List of wrapers:
```shell
karnada@mpsd-hpc-login1:~$ l $MPSD_OPENMPI_ROOT/share/openmpi/mpi*wrapper-data*
.rw-r--r--@ 2.7k mpsddeb cfel  6 Nov  2023  /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/openmpi-4.1.4-xgqifwa4vxkifktjbasgjvckd6tyynkw/share/openmpi/mpic++-wrapper-data.txt
.rw-r--r--@ 2.7k mpsddeb cfel  6 Nov  2023  /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/openmpi-4.1.4-xgqifwa4vxkifktjbasgjvckd6tyynkw/share/openmpi/mpicc-wrapper-data.txt
lrwxrwxrwx@   23 mpsddeb cfel  6 Nov  2023  /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/openmpi-4.1.4-xgqifwa4vxkifktjbasgjvckd6tyynkw/share/openmpi/mpiCC-wrapper-data.txt -> mpic++-wrapper-data.txt
lrwxrwxrwx@   23 mpsddeb cfel  6 Nov  2023  /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/openmpi-4.1.4-xgqifwa4vxkifktjbasgjvckd6tyynkw/share/openmpi/mpicxx-wrapper-data.txt -> mpic++-wrapper-data.txt
lrwxrwxrwx@   24 mpsddeb cfel  6 Nov  2023  /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/openmpi-4.1.4-xgqifwa4vxkifktjbasgjvckd6tyynkw/share/openmpi/mpif77-wrapper-data.txt -> mpifort-wrapper-data.txt
lrwxrwxrwx@   24 mpsddeb cfel  6 Nov  2023  /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/openmpi-4.1.4-xgqifwa4vxkifktjbasgjvckd6tyynkw/share/openmpi/mpif90-wrapper-data.txt -> mpifort-wrapper-data.txt
.rw-r--r--@ 2.8k mpsddeb cfel  6 Nov  2023  /opt_mpsd/linux-debian11/23b/sandybridge/spack/opt/spack/linux-debian11-sandybridge/gcc-11.3.0/openmpi-4.1.4-xgqifwa4vxkifktjbasgjvckd6tyynkw/share/openmpi/mpifort-wrapper-data.txt
```

We also ran a series of builds with  `-fprofile-update=single` and the resulting builds are running at comparable wall time as from easybuild ones. (See runs `8_default_with_single_prof_update*` in the analysis notebook)

We conclude 3 possible fixes:
	- update openmpi spack package
	- patch openmpi wrappers after toolchain installation
	- use `-`fprofiling-step=single` After discussing this with the octopus developer on 23/01/24, we decided to go with the last option as it is the least intrusive and does not require any changes to the toolchain or the build system.

If further analysis is needed, the artifacts used for the analysis are uploaded as a zip in a keeper library [here](https://keeper.mpdl.mpg.de/d/99032f1d2e8541a29ea5/) which contains the full data for all the runs and the analysis notebook can be used to analyze the data.


