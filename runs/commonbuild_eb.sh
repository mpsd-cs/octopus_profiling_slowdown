#!/bin/bash --login
# Assuming that we are running from octopus/runs/slurmwrkdir/commonbuild.sh
module purge
export EASYBUILD_PREFIX=/opt_buildbot/linux-debian11/sandybridge/EasyBuild
export MODULEPATH=/etc/lmod/modules:/usr/share/lmod/lmod/modulefiles:/usr/share/modules/modulefiles
eval `/usr/share/lmod/lmod/libexec/lmod sh use $EASYBUILD_PREFIX/2022a/modules/Core `

if [[ "$TOOLCHAINTYPE" == "mpi" ]]; then
  eval `/usr/share/lmod/lmod/libexec/lmod sh load foss Octopus/full-mpi `
  export CFLAGS="-Wall -O2 -march=native"
  export CXXFLAGS="-Wall -O2 -march=native"
  export FCFLAGS="-Wall -Wno-maybe-uninitialized -Wno-surprising -O2 -march=native -fbacktrace -fallow-argument-mismatch -ffree-line-length-none"
  export FCFLAGS_ELPA="-I${EBROOTELPA}/include/elpa-${EBVERSIONELPA}/modules"
  export MPIEXEC="${EBROOTOPENMPI}/bin/mpiexec --map-by socket"
else
  eval `/usr/share/lmod/lmod/libexec/lmod sh load foss Octopus/full `
  export CFLAGS="-Wall -O2 -march=native "
  export CXXFLAGS="-Wall -O2 -march=native "
  export FCFLAGS="-Wall -Wno-maybe-uninitialized -Wno-surprising -O2 -march=native -fbacktrace "
fi

############# ASHWINS STUFF ################
if [[ $workmode != "profile" ]]; then
  echo "Non profiling mode: not setting profiling flags"
else
  echo "Profiling mode: setting profiling flags"
  export FCFLAGS="$FCFLAGS -ftest-coverage -fprofile-arcs"
  export CFLAGS="$CFLAGS -ftest-coverage -fprofile-arcs"
  export CXXFLAGS="$CXXFLAGS -ftest-coverage -fprofile-arcs"
fi
# ### add forcibly -pthreads
# export CFLAGS="$CFLAGS -pthread"
# export CXXFLAGS="$CXXFLAGS -pthread"
# export FCFLAGS="$FCFLAGS -pthread"
# export LDFLAGS="$LDFLAGS -pthread"


if [[ "$TOOLCHAINTYPE" == "mpi" ]]; then
  ../../configure --with-libxc-prefix=$EBROOTLIBXC --with-gsl-prefix=$EBROOTGSL --with-netcdf-prefix=$EBROOTNETCDFMINFORTRAN --with-etsf-io-prefix=$EBROOTETSF_IO --with-sparskit=$EBROOTSPARSKIT2/lib/libskit.a --with-nlopt-prefix=$EBROOTNLOPT --with-dftbplus-prefix=$EBROOTDFTBPLUS --with-berkeleygw-prefix=$EBROOTBERKELEYGW --with-psolver-prefix=$EBROOTPSOLVER --with-futile-prefix=$EBROOTFUTILE --with-pspio-prefix=$EBROOTLIBPSPIO --with-fftw-prefix=$EBROOTFFTW --with-poke-prefix=$EBROOTPOKE --with-nfft=$EBROOTNFFT --with-libvdwxc-prefix=$EBROOTLIBVDWXC --enable-mpi --with-elpa-prefix=$EBROOTELPA --with-pfft-prefix=$EBROOTPFFT --with-pnfft-prefix=$EBROOTPNFFT --with-blas="-L$BLAS_LIB_DIR $LIBBLAS" --with-blacs="-L$SCALAPACK_LIB_DIR $LIBSCALAPACK" --prefix=`pwd`/installed| tee 00-configure.log 2>&1
else
  ../../configure --with-libxc-prefix=$EBROOTLIBXC --with-gsl-prefix=$EBROOTGSL --with-netcdf-prefix=$EBROOTNETCDFMINFORTRAN --with-etsf-io-prefix=$EBROOTETSF_IO --with-sparskit=$EBROOTSPARSKIT2/lib/libskit.a --with-nlopt-prefix=$EBROOTNLOPT --with-dftbplus-prefix=$EBROOTDFTBPLUS --with-berkeleygw-prefix=$EBROOTBERKELEYGW --with-psolver-prefix=$EBROOTPSOLVER --with-futile-prefix=$EBROOTFUTILE --with-pspio-prefix=$EBROOTLIBPSPIO --with-fftw-prefix=$EBROOTFFTW --with-poke-prefix=$EBROOTPOKE --with-nfft=$EBROOTNFFT --with-libvdwxc-prefix=$EBROOTLIBVDWXC --with-blas="-L$BLAS_LIB_DIR $LIBBLAS"
fi
echo "-------------------------------------------------------------------------------" >&2
echo "configure output has been saved to 00-configure.log" >&2
if [ "x${GCC_ARCH-}" = x ] ; then
    echo "Microarchitecture optimization: native (set \$GCC_ARCH to override)" >&2
else
    echo "Microarchitecture optimization: $GCC_ARCH (from override \$GCC_ARCH)" >&2
fi
echo "-------------------------------------------------------------------------------" >&2

make -j 16
make install
############# ASHWINS STUFF ################
if [[ "$TOOLCHAINTYPE" == "mpi" ]]; then
  export OCT_TEST_NJOBS=8
else
  export OCT_TEST_NJOBS=16
fi
export OMP_NUM_THREADS=1
# export OCT_ProfilingMode=prof_memory_full
export TEMPDIRPATH="${PWD}/makecheckdump"
export TMPDIR=${TEMPDIRPATH}
make check
