#!/bin/bash --login
# use SPACK toolchain but with EasyBuild compilers
export EASYBUILD_PREFIX=/opt_buildbot/linux-debian11/sandybridge/EasyBuild
eval `/usr/share/lmod/lmod/libexec/lmod sh use $EASYBUILD_PREFIX/2022a/modules/Core `
eval `/usr/share/lmod/lmod/libexec/lmod sh load GCC/11.3.0 OpenMPI ScaLAPACK/2.2.0-fb PFFT PNFFT ELPA `


if [[ "$TOOLCHAINTYPE" == "mpi" ]]; then
    export CC="/opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022a/software/OpenMPI/4.1.4-GCC-11.3.0/bin/mpicc"
    MARCH_FLAG="-march=${GCC_ARCH:-native}"
    OPTIMISATION_LEVEL="-O2"
    export CFLAGS="$MARCH_FLAG $OPTIMISATION_LEVEL -g -fno-var-tracking-assignments"
    export CXX="/opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022a/software/OpenMPI/4.1.4-GCC-11.3.0/bin/mpicxx"
    export CXXFLAGS="$MARCH_FLAG $OPTIMISATION_LEVEL -g -fno-var-tracking-assignments"
    export FC="/opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022a/software/OpenMPI/4.1.4-GCC-11.3.0/bin/mpif90"
    export FCFLAGS="$MARCH_FLAG $OPTIMISATION_LEVEL -g -fno-var-tracking-assignments -ffree-line-length-none -fallow-argument-mismatch -fallow-invalid-boz"
else
    export CC="/opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022a/software/GCCcore/11.3.0/bin/gcc"
    MARCH_FLAG="-march=${GCC_ARCH:-native}"
    OPTIMISATION_LEVEL="-O2"
    export CFLAGS="$MARCH_FLAG $OPTIMISATION_LEVEL -g -fno-var-tracking-assignments"
    export CXX="/opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022a/software/GCCcore/11.3.0/bin/g++"
    export CXXFLAGS="$MARCH_FLAG $OPTIMISATION_LEVEL -g -fno-var-tracking-assignments"
    export FC="/opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022a/software/GCCcore/11.3.0/bin/gfortran"
    export FCFLAGS="$MARCH_FLAG $OPTIMISATION_LEVEL -g -fno-var-tracking-assignments -ffree-line-length-none -fallow-argument-mismatch -fallow-invalid-boz"

fi

############# ASHWINS STUFF ################
if [[ $workmode != "profile" ]]; then
  echo "Non profiling mode: not setting profiling flags"
else
  echo "Profiling mode: setting profiling flags"
  export FCFLAGS="$FCFLAGS -ftest-coverage -fprofile-arcs"
  export CFLAGS="$CFLAGS -ftest-coverage -fprofile-arcs"
  export CXXFLAGS="$CXXFLAGS -ftest-coverage -fprofile-arcs"
fi

#HG: ugly hack to include rpath while linking
#    becomes necessary for spack >= 0.19, as it does not set LD_LIBRARY_PATH anymore
export LDFLAGS=`echo ${LIBRARY_PATH:+:$LIBRARY_PATH} | sed -e 's/:/ -Wl,-rpath=/g'`

# HG: help configure to find the ELPA F90 modules and libraries
try_mpsd_elpa_version=`expr match "$MPSD_ELPA_ROOT" '.*/elpa-\([0-9.]\+\)'`
if [ -n "$try_mpsd_elpa_version" ] ; then
    if [ -r $MPSD_ELPA_ROOT/include/elpa_openmp-$try_mpsd_elpa_version/modules/elpa.mod ]; then
        export FCFLAGS_ELPA="-I$MPSD_ELPA_ROOT/include/elpa_openmp-$try_mpsd_elpa_version/modules"
        export LIBS_ELPA="-lelpa_openmp"
    elif [ -r $MPSD_ELPA_ROOT/include/elpa-$try_mpsd_elpa_version/modules/elpa.mod ] ; then
        export FCFLAGS_ELPA="-I$MPSD_ELPA_ROOT/include/elpa-$try_mpsd_elpa_version/modules"
    fi
fi
unset try_mpsd_elpa_version


# HG: always keep options in the order listed by ``configure --help``
if [[ "$TOOLCHAINTYPE" == "mpi" ]]; then
    ../../configure \
        --enable-mpi \
        --enable-openmp \
        --with-libxc-prefix="$EBROOTLIBXC" \
        --with-libvdwxc-prefix="$EBROOTLIBVDWXC" \
        --with-blas="-L$MPSD_OPENBLAS_ROOT/lib -lopenblas" \
        --with-gsl-prefix="$MPSD_GSL_ROOT" \
        --with-fftw-prefix="$MPSD_FFTW_ROOT" \
        --with-pfft-prefix="$EBROOTPFFT" \
        --with-nfft="$MPSD_NFFT_ROOT" \
        --with-pnfft-prefix="$EBROOTPNFFT" \
        --with-berkeleygw-prefix="$MPSD_BERKELEYGW_ROOT" \
        --with-sparskit="$MPSD_SPARSKIT_ROOT/lib/libskit.a" \
        --with-nlopt-prefix="$MPSD_NLOPT_ROOT" \
        --with-blacs="-L$SCALAPACK_LIB_DIR $LIBSCALAPACK" \
        --with-scalapack="-L$SCALAPACK_LIB_DIR $LIBSCALAPACK" \
        --with-elpa-prefix="$EBROOTELPA" \
        --with-cgal="$MPSD_CGAL_ROOT" \
        --with-boost="$MPSD_BOOST_ROOT" \
        --with-psolver-prefix="$MPSD_BIGDFT_PSOLVER_ROOT" \
        --with-futile-prefix="$MPSD_BIGDFT_FUTILE_ROOT" \
        --with-atlab-prefix="$MPSD_BIGDFT_ATLAB_ROOT" \
        --with-dftbplus-prefix="$MPSD_DFTBPLUS_ROOT" \
        --with-netcdf-prefix="$MPSD_NETCDF_FORTRAN_ROOT" \
        --with-etsf-io-prefix="$MPSD_ETSF_IO_ROOT" \
        --prefix=`pwd`/install \
        "$@" | tee 00-configure.log 2>&1
else
    ../../configure \
        --enable-openmp \
        --with-libxc-prefix="$EBROOTLIBXC" \
        --with-libvdwxc-prefix="$EBROOTLIBVDWXC" \
        --with-blas="-L$MPSD_OPENBLAS_ROOT/lib -lopenblas" \
        --with-gsl-prefix="$MPSD_GSL_ROOT" \
        --with-fftw-prefix="$MPSD_FFTW_ROOT" \
        --with-nfft="$MPSD_NFFT_ROOT" \
        --with-netcdf-prefix="$MPSD_NETCDF_ROOT" \
        --with-etsf-io-prefix="$MPSD_ETSF_IO_ROOT" \
        --with-berkeleygw-prefix="$MPSD_BERKELEYGW_ROOT" \
        --with-sparskit="$MPSD_SPARSKIT_ROOT/lib/libskit.a" \
        --with-nlopt-prefix="$MPSD_NLOPT_ROOT" \
        --with-cgal="$MPSD_CGAL_ROOT" \
        --with-boost="$MPSD_BOOST_ROOT" \
        --with-psolver-prefix="$MPSD_BIGDFT_PSOLVER_ROOT" \
        --with-futile-prefix="$MPSD_BIGDFT_FUTILE_ROOT" \
        --with-atlab-prefix="$MPSD_BIGDFT_ATLAB_ROOT" \
        --with-dftbplus-prefix="$MPSD_DFTBPLUS_ROOT" \
        --with-netcdf-prefix="$MPSD_NETCDF_FORTRAN_ROOT" \
        --with-etsf-io-prefix="$MPSD_ETSF_IO_ROOT" \
        "$@" | tee 00-configure.log 2>&1
fi
echo "-------------------------------------------------------------------------------" >&2
echo "configure output has been saved to 00-configure.log" >&2
if [ "x${GCC_ARCH-}" = x ] ; then
    echo "Microarchitecture optimization: native (set \$GCC_ARCH to override)" >&2
else
    echo "Microarchitecture optimization: $GCC_ARCH (from override \$GCC_ARCH)" >&2
fi
echo "-------------------------------------------------------------------------------" >&2

make -j 16
make install

############# ASHWINS STUFF ################
if [[ "$TOOLCHAINTYPE" == "mpi" ]]; then
    export OCT_TEST_NJOBS=8
    export OMP_NUM_THREADS=1
else
    export OCT_TEST_NJOBS=16
    export OMP_NUM_THREADS=1
fi
# export OCT_ProfilingMode=prof_memory_full
export MPIEXEC="orterun --map-by socket"
export TEMPDIRPATH_out="${PWD}/makecheckdump"
export TEMPDIRPATH="/tmp/makecheckdump"
export TMPDIR=${TEMPDIRPATH}
make check

mv ${TEMPDIRPATH} ${TEMPDIRPATH_out}
