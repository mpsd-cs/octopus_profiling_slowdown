#!/bin/bash --login
# Assuming that we are running from octopus/runs/slurmwrkdir/commonbuild.sh

if [[ "$TOOLCHAINTYPE" == "mpi" ]]; then
    export LDFLAGS=`echo ${LIBRARY_PATH:+:$LIBRARY_PATH} | sed -e 's/:/ -Wl,-rpath=/g'`

    export LIBBLAS="-lopenblas"
    export LIBSCALAPACK="-lscalapack"
    export LIBS_BERKELEYGW="-L/lib -lBGW_wfn -lgomp"
    export LIBS_ELPA="-lelpa_openmp"

    export BLAS_LIB_DIR=$MPSD_OPENBLAS_ROOT/lib
    export CFLAGS="-Wall -O2 -march=native "
    export CXXFLAGS="-Wall -O2 -march=native "
    export FCFLAGS="-Wall -Wno-maybe-uninitialized -Wno-surprising -O2 -march=native -fbacktrace -ffree-line-length-none"
    export CC="mpicc"
    export CXX="mpicxx"
    export FC="mpif90"
else

    export LDFLAGS=`echo ${LIBRARY_PATH:+:$LIBRARY_PATH} | sed -e 's/:/ -Wl,-rpath=/g'`

    export LIBBLAS="-lopenblas"

    export BLAS_LIB_DIR=$MPSD_OPENBLAS_ROOT/lib
    export CFLAGS="-Wall -O2 -march=native "
    export CXXFLAGS="-Wall -O2 -march=native "
    export FCFLAGS="-Wall -Wno-maybe-uninitialized -Wno-surprising -O2 -march=native -fbacktrace -fallow-argument-mismatch -ffree-line-length-none"
    export MPIEXEC="orterun --map-by socket"

    export CC="gcc"
    export CXX="g++"
    export FC="gfortran"
fi

############# ASHWINS STUFF ################
if [[ $workmode != "profile" ]]; then
  echo "Non profiling mode: not setting profiling flags"
else
  echo "Profiling mode: setting profiling flags"
  export FCFLAGS="$FCFLAGS -ftest-coverage -fprofile-arcs -fprofile-update=single"
  export CFLAGS="$CFLAGS -ftest-coverage -fprofile-arcs -fprofile-update=single"
  export CXXFLAGS="$CXXFLAGS -ftest-coverage -fprofile-arcs -fprofile-update=single"
fi
### add forcibly -pthreads
# export CFLAGS="$CFLAGS -pthread"
# export CXXFLAGS="$CXXFLAGS -pthread"
# export FCFLAGS="$FCFLAGS -pthread"
# export LDFLAGS="$LDFLAGS -pthread"
#HG: ugly hack to include rpath while linking
#    becomes necessary for spack >= 0.19, as it does not set LD_LIBRARY_PATH anymore
export LDFLAGS=`echo ${LIBRARY_PATH:+:$LIBRARY_PATH} | sed -e 's/:/ -Wl,-rpath=/g'`

# HG: help configure to find the ELPA F90 modules and libraries
try_mpsd_elpa_version=`expr match "$MPSD_ELPA_ROOT" '.*/elpa-\([0-9.]\+\)'`
if [ -n "$try_mpsd_elpa_version" ] ; then
    if [ -r $MPSD_ELPA_ROOT/include/elpa_openmp-$try_mpsd_elpa_version/modules/elpa.mod ]; then
        export FCFLAGS_ELPA="-I$MPSD_ELPA_ROOT/include/elpa_openmp-$try_mpsd_elpa_version/modules"
        export LIBS_ELPA="-lelpa_openmp"
    elif [ -r $MPSD_ELPA_ROOT/include/elpa-$try_mpsd_elpa_version/modules/elpa.mod ] ; then
        export FCFLAGS_ELPA="-I$MPSD_ELPA_ROOT/include/elpa-$try_mpsd_elpa_version/modules"
    fi
fi
unset try_mpsd_elpa_version


# HG: always keep options in the order listed by ``configure --help``
if [[ "$TOOLCHAINTYPE" == "mpi" ]]; then
    export FCFLAGS_ELPA="-I${MPSD_ELPA_ROOT}/include/elpa_openmp-2021.11.001/modules"
    export SCALAPACK_LIB_DIR="${MPSD_NETLIB_SCALAPACK_ROOT}/lib"
    export LIBS_FUTILE="-lgomp -L${MPSD_BIGDFT_FUTILE_ROOT}/lib -lfutile-1 -lyaml"
    export LIBS_ATLAB="-L${MPSD_BIGDFT_ATLAB_ROOT}/lib -latlab-1"
    export LIBS_PSOLVER="-L${MPSD_BIGDFT_PSOLVER_ROOT}/lib -lPSolver-1 -lgomp"
    ../../configure --with-libxc-prefix=$MPSD_LIBXC_ROOT --with-gsl-prefix=$MPSD_GSL_ROOT --with-netcdf-prefix=$MPSD_NETCDF_FORTRAN_ROOT --with-etsf-io-prefix=$MPSD_ETSF_IO_ROOT --with-sparskit=$MPSD_SPARSKIT_ROOT/lib/libskit.a --with-nlopt-prefix=$MPSD_NLOPT_ROOT --with-dftbplus-prefix=$MPSD_DFTBPLUS_ROOT --with-berkeleygw-prefix=$MPSD_BERKELEYGW_ROOT --with-psolver-prefix=$MPSD_BIGDFT_PSOLVER_ROOT --with-futile-prefix=$MPSD_BIGDFT_FUTILE_ROOT --with-pspio-prefix=$MPSD_LIBPSPIO_ROOT --with-fftw-prefix=$MPSD_FFTW_ROOT --with-poke-prefix=$MPSD_POKE_ROOT --with-nfft=$MPSD_NFFT_ROOT --with-libvdwxc-prefix=$MPSD_LIBVDWXC_ROOT --enable-mpi --with-elpa-prefix=$MPSD_ELPA_ROOT --with-pfft-prefix=$MPSD_PFFT_ROOT --with-pnfft-prefix=$MPSD_PNFFT_ROOT --with-blas="-L$BLAS_LIB_DIR $LIBBLAS" --with-blacs="-L$SCALAPACK_LIB_DIR $LIBSCALAPACK" --with-atlab-prefix=$MPSD_BIGDFT_ATLAB_ROOT --prefix=`pwd`/install  | tee 00-configure.log 2>&1
else
    export LIBS_FUTILE="-lgomp -L${MPSD_BIGDFT_FUTILE_ROOT}/lib -lfutile-1 -lyaml"
    export LIBS_ATLAB="-L${MPSD_BIGDFT_ATLAB_ROOT}/lib -latlab-1"
    export LIBS_PSOLVER="-L${MPSD_BIGDFT_PSOLVER_ROOT}/lib -lPSolver-1 -lgomp"
    ../../configure --with-libxc-prefix=$MPSD_LIBXC_ROOT --with-gsl-prefix=$MPSD_GSL_ROOT --with-netcdf-prefix=$MPSD_NETCDF_FORTRAN_ROOT --with-etsf-io-prefix=$MPSD_ETSF_IO_ROOT --with-sparskit=$MPSD_SPARSKIT_ROOT/lib/libskit.a --with-nlopt-prefix=$MPSD_NLOPT_ROOT --with-dftbplus-prefix=$MPSD_DFTBPLUS_ROOT --with-berkeleygw-prefix=$MPSD_BERKELEYGW_ROOT --with-psolver-prefix=$MPSD_BIGDFT_PSOLVER_ROOT --with-futile-prefix=$MPSD_BIGDFT_FUTILE_ROOT --with-pspio-prefix=$MPSD_LIBPSPIO_ROOT --with-fftw-prefix=$MPSD_FFTW_ROOT --with-poke-prefix=$MPSD_POKE_ROOT --with-nfft=$MPSD_NFFT_ROOT --with-libvdwxc-prefix=$MPSD_LIBVDWXC_ROOT --with-blas="-L$BLAS_LIB_DIR $LIBBLAS" --with-atlab-prefix=$MPSD_BIGDFT_ATLAB_ROOT --prefix=`pwd`/install  | tee 00-configure.log 2>&1
fi
echo "-------------------------------------------------------------------------------" >&2
echo "configure output has been saved to 00-configure.log" >&2
if [ "x${GCC_ARCH-}" = x ] ; then
    echo "Microarchitecture optimization: native (set \$GCC_ARCH to override)" >&2
else
    echo "Microarchitecture optimization: $GCC_ARCH (from override \$GCC_ARCH)" >&2
fi
echo "-------------------------------------------------------------------------------" >&2

make -j 16
make install

############# ASHWINS STUFF ################
if [[ "$TOOLCHAINTYPE" == "mpi" ]]; then
    export OCT_TEST_NJOBS=8
    export OMP_NUM_THREADS=1
else
    export OCT_TEST_NJOBS=16
    export OMP_NUM_THREADS=1
fi
# export OCT_ProfilingMode=prof_memory_full
export MPIEXEC="orterun --map-by socket"
export TEMPDIRPATH_out="${PWD}/makecheckdump"
# export TEMPDIRPATH="/tmp/makecheckdump"
export TEMPDIRPATH=${TEMPDIRPATH_out}
export TMPDIR=${TEMPDIRPATH}
make check

# mv ${TEMPDIRPATH} ${TEMPDIRPATH_out}
