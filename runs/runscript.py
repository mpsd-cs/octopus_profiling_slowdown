"""
A python script that takes :
- run number : A number representing run
- metadata: Arbitrary metadata for inforamtion
- profiling: A boolean to enable profiling
- oct_profiling: A boolean to enable octopus profiling
- toolchain: 2021a or 2022a
- dry_run: A boolean to print the slurm script but do not submit it
- eb: A boolean to run on eb toolchain
- serial: A boolean to run on serial toolchain (only spack toolchain)
and then generates and submits a slurm job to compile and run the octopus code checks
"""

import argparse
import os
import subprocess
import sys

def generate_slurm_script(run_number: int, metadata: str, profiling: bool, oct_profiling: bool, toolchain: str, dry_run: bool, eb: bool=False, serial: bool=False):
    """
    Generates a slurm script to compile and run the octopus code checks

    Parameters:
    - run_number (int): The run number.
    - metadata (str): The metadata.
    - profiling (bool): Flag indicating whether profiling is enabled.
    - oct_profiling (bool): Flag indicating whether octopus profiling is enabled.
    - toolchain (str): The toolchain.
    - dry_run (bool): Flag indicating whether it is a dry run.
    - eb (bool, optional): Flag indicating whether to use commonbuild_eb.sh. Defaults to False.
    - serial (bool, optional): Flag indicating whether to use serial toolchain. Defaults to False.
    """
    workmode = "profile" if profiling else "no_profile"
    oct_profiling_mode = "octprof" if oct_profiling else "nooctprof"
    slurm_workdir = f"{run_number}_{metadata}_{toolchain}_{workmode}_{oct_profiling_mode}"
    slurm_out_file = slurm_workdir + "-%j.logit"
    slurm_jobname = slurm_workdir
    tolchain_type = "serial" if serial else "mpi"
    toolchain_to_load = f"foss2021a-{tolchain_type}" if "21" in toolchain else f"foss2022a-{tolchain_type}"
    set_oct_profiling = "export OCT_ProfilingMode=prof_time" if oct_profiling else ""
    set_workmode = 'export workmode="profile"' if profiling else 'export workmode="no_profile"'

    commonbuild_script = "commonbuild.sh" if not eb else "commonbuild_eb.sh"
    # make slurm_workdir and if fails exit the program
    if not dry_run:
        try:
            os.mkdir(slurm_workdir)
        except OSError:
            print(f"Creation of the directory {slurm_workdir} failed")
            sys.exit(1)
    slurm_script = f"""#!/bin/bash --login
#
# Standard output and error
#SBATCH -o {slurm_out_file}
#
# working directory
#SBATCH -D {slurm_workdir}
#
# partition
#SBATCH -p public
#
# job name
#SBATCH -J  {slurm_jobname}
#
#
# job requirements
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
#SBATCH --time=05:10:00
export TOOLCHAINTYPE={tolchain_type}

mpsd-modules 23b
# module use /opt_mpsd/linux-debian11/ashwins_playground/test-23c-witheb-mpi-wrapper/23c/sandybridge/lmod/Core
module load toolchains/{toolchain_to_load}
{set_workmode}
{set_oct_profiling}
cp ../{commonbuild_script} .
source {commonbuild_script}
"""
    if dry_run:
        print(slurm_script)
        return
    with open(slurm_workdir + ".sh", "w") as f:
        f.write(slurm_script)
    subprocess.run(["sbatch", slurm_workdir + ".sh"])

def main():
    parser = argparse.ArgumentParser(description='Generate and submit a slurm job to compile and run the octopus code checks')
    parser.add_argument('run_number', type=int, help='A number representing run')
    parser.add_argument('metadata', type=str, help='Arbitrary metadata for inforamtion')
    parser.add_argument('--profiling', action='store_true', help='Enable profiling')
    parser.add_argument('--oct_profiling', action='store_true', help='Enable octopus profiling')
    parser.add_argument('--toolchain', type=str, default="2021a", help='2021a or 2022a')
    parser.add_argument('--dry_run', action='store_true', help='Dry run, print the slurm script but do not submit it')
    parser.add_argument('--eb', action='store_true', help='run on eb toolchain')
    parser.add_argument('--serial', action='store_true', help='run on serial toolchain')
    args = parser.parse_args()
    generate_slurm_script(args.run_number, args.metadata, args.profiling, args.oct_profiling, args.toolchain, args.dry_run, args.eb, args.serial)

if __name__ == "__main__":
    main()